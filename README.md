# OPOR Email Signature Repo
Git repo for building the html email signature for OPOR client.

Based on Foundation Email: [https://get.foundation/emails.html](https://get.foundation/emails.html)

## Documentation to follow for html and css:
[https://get.foundation/emails/docs/](https://get.foundation/emails/docs/)

## Workflow
1. write html in index.html
2. customize css in css/foundation-email.css
3. Inline html and css using Foundation Inliner tool: [https://get.foundation/emails/inliner.html](https://get.foundation/emails/inliner.html)
4. Put inlined code in opor-email-signature.html file and share with client
